let contador = JSON.parse(localStorage.getItem('contadorData'))

const tCompras =document.getElementById('tcompras')
const templateComp = document.getElementById('template-tcomp').content

const fragment = document.createDocumentFragment()

const pintarcompras = ()=>{
    tcompras.innerHTML =''
    Object.values(contador).forEach(producto =>{
        templateComp.querySelector('th').textContent = producto.id
        templateComp.querySelector('td').textContent = producto.titulo
        templateComp.querySelector('td').textContent = producto.cantidad

        const clone = templateComp.cloneNode(true)
        fragment.appendChild(clone)
    })
    tCompras.appendChild(fragment)
}
pintarcompras()