const toTop = document.getElementById('btnToTop')
const items = document.getElementById('items')
const templateCard = document.getElementById('template-card').content

const contadores = document.getElementById('contadores')
const templateCont = document.getElementById('template-cont').content

const fragment = document.createDocumentFragment()

let contador = {}

//const fragmentc = document.createDocumentFragment()
document.addEventListener('DOMContentLoaded', () => {
    fetchData()
})

// creamos un evento para adicionar al contador 
items.addEventListener('click', e => {
    addContador(e)
})
// Acción boton volver al inicio
toTop.addEventListener('click', e =>{
    scrollToTop()
})

const scrollToTop = ()=>{
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });
}



const fetchData = async () => {
    try {
        const res = await fetch('./Json/api.json')
        const data = await res.json()
        //console.log(data)
        pintarcards(data)

    } catch (error) {
        console.log(error)
    }
}

const pintarcont = () => {
    //console.log(contador)
    contadores.innerHTML = ''
    Object.values(contador).forEach(producto => {
        templateCont.querySelector('h4').textContent = producto.titulo
        templateCont.querySelector('p').textContent = producto.cantidad

        const clone = templateCont.cloneNode(true)
        fragment.appendChild(clone)
    })
    
    contadores.appendChild(fragment)
}
const pintarcards = data => {
    data.forEach(producto => {
        templateCard.querySelector('h5').textContent = producto.titulo
        templateCard.querySelector('p').textContent = producto.precio
        templateCard.querySelector('img').setAttribute('src', producto.img)
        templateCard.querySelector('.btn-primary').dataset.id = producto.id



        const clone = templateCard.cloneNode(true)
        fragment.appendChild(clone)

    });
    items.appendChild(fragment)

}

const addContador = e => {
    //selecciona cualquier elemento de la card
    //console.log(e.target)
    //solo cuando se de click en el boton retorna true
    // todo lo demas es falso por eso creamos el if
    //console.log(e.target.classList.contains('btn-primary'))
    if (e.target.classList.contains('btn-primary')) {
        // como se puede apreciar, tomamos toda la informacion de la card al click al boton

        //console.log(e.target.parentElement)

        //con la siguiente linea se envia la informacion del boton a la funcion setcontador
        setContador(e.target.parentElement)

    }
    // evita que los eventos realizados en esta funcion se propagen a otros 
    e.stopPropagation()
}

const setContador = objeto => {
    // en esta funcion recibimos los valores de la card seleccionada
    //console.log(objeto)
    //creaomos una constante donde vamos a almacenar los valores de interes 
    // de la card seleccionada 
    const producto = {
        id: objeto.querySelector('.btn-primary').dataset.id,
        titulo: objeto.querySelector('h5').textContent,
        cantidad: 1
    }
    // antes de seguir debemos crear una variable para almacenar el producto 
    // despues evaluamos si ya esta almacenado el id 
    if (contador.hasOwnProperty(producto.id)) {
        // en caso de estarlo aumentamos la cantidad
        producto.cantidad = contador[producto.id].cantidad + 1
    }
    // si no esta creamos un nuevo contador para el id dado
    contador[producto.id] = { ...producto }
    //comprobamos el funcionamiento con la siguiente liena
    //console.log(producto)
    // despues de comprobarlo agregamas el llamado a la funcion pintar contador
    //pintarcont()
    enviarCont()
}
const enviarCont = () => {
    console.log(contador)
    localStorage.setItem('contadorData',JSON.stringify(contador))
}
